function SendForm(){
    var email = $('#email-invitations').val();
    var projectId = $('buttom').attr('id');
    var url = $('.invitation').attr('action');
    $.post(url, {email:email, projectId:projectId}, function(data){
        if('ok' == data.status){
            $('#email-invitations').val('');
        }else{
            alert('Ошибка при отправке приглашения!');
        }
    });
}
function updateRatingForFeature(){
    $('.selected .rating').last().raty({
        score: function() {
            return $(this).attr('data-score');}, 
        readOnly: true,
        path: window.baseUrl+"/img"
    });
}
function featureScore(id){
    $('#feature-score').last().raty({ 
        score: function() {
            return $(this).attr('data-score');}, 
        path: window.baseUrl+"/img",
        click: function(score, evt) {
            var featureID = $(this).attr('data-feature-id');
            var userId = id;
            $.post(window.baseUrl+'/rating', {featureID:featureID, userId:userId, score:score},function(data){
                if(data.status == 'ok'){
                    $('.selected .rating').attr('data-score', data.score);
                    updateRatingForFeature();
                }
            });
        }
    });
}
$(function(){
    $("form").keypress(function (event) {if (event.keyCode == 13) {event.preventDefault();SendForm();}});
    $('.data-feature').click(function(){
        var featureId = $(this).attr('data-feature-id');
        $('#feature-score').attr('data-feature-id',featureId);
        $.post(window.baseUrl+'/rating/get', {featureID:$(this).attr('data-feature-id')}, function(data){
            $('#feature-score').attr('data-score', data.score);
            featureScore(data.userId);
        });
    });
    $('#feature-score').hide();
    var leftFloat = $('.floating').offset().left;
    var topFloat = $('.floating').offset().top;
    $(window).resize(function (){
        leftFloat = $('.floating').offset().left;
        topFloat = $('.floating').offset().top;
    });
    $(window).scroll(function() { 
        var top = $(document).scrollTop();
        if (top > 300) {
            if ($('.data-feature:last').offset().top > top) $('.floating').offset({top:top,left:leftFloat});
        } else {
            $('.floating').offset({top:topFloat,left:leftFloat});
        }
    });
})