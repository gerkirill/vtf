
;(function(window, jQuery){

/************* repository.js **************/

var Repository = function(){

	// elements added to the repository
	var _elements = [];
	// 'field' => 'value' => [element IDS]
	var _indexes = {};

	function _addIndex (index, indexValue, elementKey) {
		if (typeof _indexes[index] === 'undefined') {
			_indexes[index] = {};
		}
		if (typeof _indexes[index][indexValue] === 'undefined') {
			_indexes[index][indexValue] = [];
		}
		_indexes[index][indexValue].push(elementKey);
	}

	// indexCount - object: key => matched indexes count
	// matchingElementKeys - array of integers
	function _updateMatchingIndexesCount(indexCount, matchingElementKeys) {
		for (var i=0; i<matchingElementKeys.length; i++) {
			var key = matchingElementKeys[i];
			if (typeof indexCount[key] === 'undefined') {
				indexCount[key] = 1;
			} else {
				indexCount[key]++;
			}
		}
		return indexCount;
	}

	// object ids key-id, value- their count
	function _findIdsMatchedXTimes(ids, matchesRequired) {
		var resultIds = [];
		for (id in ids) {
			var matchedTimes = ids[id];
			if (matchedTimes === matchesRequired) {
				resultIds.push(id);
			}
		}
		return resultIds;
	}

	function _findElementsWhichExistByIds(elementIds) {
		var elements = [];
		for (var i=0; i<elementIds.length; i++) {
			var elementId = elementIds[i];
			if (typeof _elements[elementId] !== 'undefined') {
				elements.push(_elements[elementId]);
			}
		}
		return elements;
	}

	return {
		/**
		 * adds element to the repository, also adds it to all the indexes for the search
		 */
		add: function(element, indexes) {
			var newLength = _elements.push(element);
			var elementId = newLength - 1;
			for (var index in indexes) {
				var indexValue = indexes[index];
				_addIndex(index, indexValue, elementId);
			}
		},

		/*
		 * finds element in the repository by one or multiple key=>value pairs
		 */
		find: function(criteria) {
			// array of elements from the repository which match all the key->value pairs
			var matchingElements = [];
			// key - element index, value - how much criteria element with this this id matches
			var indexesMatchCount = {};
			// number of indexes mentioned in criteria
			var numberOfIndexes = 0;
			var matchingElementIds = [];
			// find matching element ids by each key, find common ones, existing in _elemets array
			for (var index in criteria) {
				numberOfIndexes++;
				var indexValue = criteria[index];
				if (typeof _indexes[index] === 'undefined') continue;
				if (typeof _indexes[index][indexValue] === 'undefined') continue;
				var idsMatchingIndex = _indexes[index][indexValue];
				indexesMatchCount = _updateMatchingIndexesCount(indexesMatchCount, idsMatchingIndex);
			}
			matchingElementIds = _findIdsMatchedXTimes(indexesMatchCount, numberOfIndexes);
			// now add elements with ids in matchingElementIds, which exist in _elements
			matchingElements = _findElementsWhichExistByIds(matchingElementIds);
			return matchingElements;
		},

		remove: function(element) {
			for (var i=0; i<_elements.length; i++) {
				if (_elements[i] === element) {
					delete _elements[i];
					return;
				}
			}
		},

		getAll: function() {
			var elements = [];
			// skip removed elements
			for (var i=0; i<_elements.length; i++) {
				if (typeof _elements[i] !== 'undefined') {
					elements.push(_elements[i]);
				}
			}
			return elements;
		},

		removeAll: function() {
			_elements = [];
			_indexes = {};
		}
	};
};

/************* event_dispatcher.js **************/

var EventDispatcher = function(Repository) {

	var _callbacks = Repository();
	var _eventStack = [];
	const _DEFER = 'defer';

	function _eventTagsToStr(eventTags) {
		var result = '';
		for (var tag in eventTags) {
			result += (tag + ':' + eventTags[tag]+';');
		}
		return result;
	}

	function _alreadyInStack(eventTags) {
		var eventTagsStr = _eventTagsToStr(eventTags);
		return _eventStack.indexOf(eventTagsStr) != -1;
	}

	function _addToEventStack(eventTags) {
		var eventTagsStr = _eventTagsToStr(eventTags);
		_eventStack.push(eventTagsStr);
	}

	/*
	function _createDeferedCallbackNotificator(deferedCallbacksCounter, afterAllCallbacksExecutedCallback) {
		return function() {
			deferedCallbacksCounter--;
			if (deferedCallbacksCounter < 0) {
				throw "Event dispatcher exception: too much notifocations from defered callbacks";
			}
			if (deferedCallbacksCounter === 0) {
				afterAllCallbacksExecutedCallback();
			}
		};
	}
	*/
	return {

		DEFER: _DEFER,

		/**
		 * Subscribe to events matching "tags"
		 */
		subscribe: function(tags, callback) {
			_callbacks.add(callback, tags);
		},
		unsubscribe: function(tags) {
			var matchingCallbacks = _callbacks.find(tags);
			for (var i=0; i<matchingCallbacks.length; i++) {
				_callbacks.remove(matchingCallbacks[i]);
			}
		},
		/**
		 * Trigger event with related data stored into 'event' variable.
		 * This will execute registered callbacks matching 'tags'. Once all subscribed callbacks executed - 
		 * 'callback' will be called (if supplied)
		 */
		trigger: function(event, tags, callback) {
			var deferedCallbacksCount = 0;

			// avoid infinite reqursion when one event rizes another and vice versa
			if (_alreadyInStack(tags)) {
				return;
			}
			_addToEventStack(tags);

			if (typeof callback === 'undefined') {
				callback = function(){};
			}
			// notificator for async. event callbacks
			
			var callThisWhenDone = function() {
				deferedCallbacksCount--;
				if (deferedCallbacksCount < 0) {
					throw "Event dispatcher exception: too much notifocations from defered callbacks";
				}
				if (deferedCallbacksCount === 0) {
					callback();
				}
			}
			
			// notificator for async. event callbacks
			//var callThisWhenDone = _createDeferedCallbackNotificator(deferedCallbacksCount, callback);
			var matchingCallbacks = _callbacks.find(tags);
			for (var i=0; i<matchingCallbacks.length; i++) {
				var callbackResult = matchingCallbacks[i](event, callThisWhenDone);
				if (callbackResult === _DEFER) {
					// heck, event callback wants to do smth. in async. way! Not a problem - for this it should just
					// return eventDispatcher.DEFER, and call function supplied in 'callThisWhenDone' parameter when done.
					deferedCallbacksCount++;
				}
			}
			if (deferedCallbacksCount === 0) {
				callback();
			}
			_eventStack.pop();
		}
	};
};

/************* module.js **************/

/*
* Implements 2 interfaces - one for ModuleRepository and another one for module-defining function.
*/
var Module = function (Repository, jQuery) {
	var _widgets = Repository();
	var _identity;
	var _eventDispatcher;
	var _definition = {
		init: function(widget){},
		uninit: function(widget){},
		loadOnEvent: function(event){return false;},
		listeners: {}
	}

	function _initWidget(widget) {
		widget.$ = jQuery(widget);
		_definition.init(widget);
	}

	function _uninitWidget(widget) {
		_definition.uninit(widget);
	}

	function _getEventTags(eventName) {
		var eventTags = {'module': null, 'event': null, 'subscriber': _identity};
		var eventNameParts = name.split('.', 2);
		if (typeof eventNameParts[1] === 'undefined') {
			eventTags['event'] = eventName;
		} else {
			eventTags['event'] = eventNameParts[1];
			eventTags['module'] = eventNameParts[0];
		}
		return eventTags;
	}

	function _addWidget(widget) {
		_initWidget(widget);
		_widgets.add(widget, {});
	}

	function _removeWidget(widget) {
		_uninitWidget(widget);
		_widgets.remove(widget);
	}

	function _replaceWidget(widget, newContent) {
		var newWidget = jQuery(newContent).replaceAll(widget);
		_removeWidget(widget);
		_addWidget(newWidget);
		return newWidget;
	}

	/* 
	* Passed to the module defining function and thus available in init() function and event-listeners.
	* allows to: 
	*  1. trigger events with module.trigger()
	*  2. add new widget with module.addWidget()
	*  3. remove widget with module.removeWidget()
	*  4. access jQuery over module.$ property
	*/
	function _getModuleEndpoint() {
		return {
			trigger: function(eventName, eventData) {
				var tags = {/*module: _identity,*/ event: eventName};
				_eventDispatcher.trigger(eventData, tags);
			},
			addWidget: _addWidget,
			removeWidget: _removeWidget,
			replaceWidget: _replaceWidget,
			'$': jQuery
		};
	}

	// if module is subscribed to some event and event is triggered - this function is used to notify
	// all the widgets module aware of this event
	function _widgetEventNotificator(event, eventCallback) {
		var widgets = _widgets.getAll();
		for (var i=0; i<widgets.length; i++) {
			var widget = widgets[i];
			eventCallback(event, widget);
		}
	}

	return {

		// method used from the module js files to define module behavior - events binding to widget html elements,
		// trigger events and react on events from other modules. Module endpoint will be passed to moduleDefinator
		// function as a single param.
		define: function(identity, moduleDefinator) {
			_identity = identity;
			var definition = moduleDefinator(_getModuleEndpoint());
			for (key in definition) {
				_definition[key] = definition[key];
			}
		},
		// adds one more html widget module is aware of and binds events to its html
		addWidget: _addWidget,
		/*
		loadWidget: function() {

		},
		*/
		// used by module repository - to inject event dispatcher (common for all modules)
		setEventDispatcher: function(eventDispatcher) {
			_eventDispatcher = eventDispatcher;
		},
		// required by the module repository - to store into repository by identity
		getIdentity: function() {
			return _identity;
		},
		// required by the module repository, after module is defined - this will register its listeners.
		// event dispatcher should be injected before that
		registerEventListeners: function() {
			for (eventName in _definition.listeners) {
				var eventTags = _getEventTags(eventName);
				var eventCallback = _definition.listeners[eventName];
				// callback should receive 2 parameters - 1:event, 2:widget to act on.
				// event comes from eventDispatcher. widget is to be added by the module
				(function (ec) { // this is important!! otherwise only last module eventCallback registered
					_eventDispatcher.subscribe(eventTags, function(event){
						_widgetEventNotificator(event, ec);
					});
				})(eventCallback);
			}
		},
		// when the module is removed from module repository - repo invokes this function to unregister
		// its listeners
		unregiterEventListeners: function() {
			var moduleEventListeners = _eventDispatcher.find({'subscriber': _identity});
			for (var i=0; i<moduleEventListeners.length; i++) {
				_eventDispatcher.remove(moduleEventListeners[i]);
			}
		}
	};
};

/************* module_repository.js **************/

/*
* Stores js modules,
* Set module up while adding it (sets event dispatcher and registers module event listeners)
* Unregisters module event listeners upon removal (can js module be removed? upon js module reload I guess)
* Allows to regiter html widgets awailable at the page in the correponding js modules
*/
var ModuleRepository = function(Repository, EventDispatcher, jQuery, Module){

	var _modules = Repository();
	var _eventDispatcher = EventDispatcher(Repository);

	/*
	* Initializes html widget if not yet initialized
	* @param widget Html element - widget root element (container) - should have data-module attribute
	* containing module class.
	*/
	function _initWidget(widget) {
		// html widget wrapped with jQuery
		var $widget = jQuery(widget);
		// check if the widget is already initializes
		if ($widget.data('module-initialized') === 'true') return;
		var moduleIdentity = $widget.data('module');
		if (undefined == moduleIdentity) {
			throw 'There is a widget with class m-module at the page, but without data-module attribute.';
		}
		if ('' == moduleIdentity) {
			throw 'There is a widget with class m-module at the page with empty data-module attribute.';
		}
		var matchingModules = _modules.find({identity: moduleIdentity});
		if (0 === matchingModules.length) {
			throw 'Can not find module "'+moduleIdentity+'", but some widget requires it.';
		}
		var module = matchingModules[0];
		//widget.$ = $widget;
		module.addWidget(widget);
		$widget.data('module-initialized', 'true');
	}

	function _add(module) {
		// add module to the _modules repository, index by identifier
		module.setEventDispatcher(_eventDispatcher);
		module.registerEventListeners();
		_modules.add(module, {identity: module.getIdentity()});
		// register module event listeners with _eventDispatcher, index by:
		// 1. owning module identifier, 2. targeted event, 3. event owner module identifier (optional)
	}

	return {
		/*
		* High-level function which defines module and adds it to module repository at once
		*/
		defineModule: function(identity, definingFunction) {
			var module = Module(Repository, jQuery);
			module.define(identity, definingFunction);
			_add(module);
		},

		/*
		* Adds module to repository and registers its event listeners with event dispatcher
		*/
		add: function(module) {
			return _add(module);
		},

		/*
		* Removes modules from repository and unregisters its event listeners from event dispatcher
		*/
		remove: function(module) {
			// unregister event listeners from _eventDispatcher
			module.unregisterEventListeners(module);
			// remove from _modules repository
			_modules.remove(module);
		},

		/*
		* Initializes html widgets at the page (html elements with m-module class and data-module attribute).
		* Already initalized widgets are just skipped
		*/
		initWidgets: function() {
			jQuery('.m-module').each(function() {
				_initWidget(this);
			});
		}
	};
};

window.modlr = ModuleRepository(Repository, EventDispatcher, jQuery, Module);
jQuery(function(){
	window.modlr.initWidgets();
});
}(window, jQuery));
