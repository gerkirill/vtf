window.modlr.defineModule('features', function(module){
    var $ = module.$;

    function _deselect(event, widget) {
        if (event.id !== widget.$.data('feature-id')) {
            widget.$.removeClass('selected');
        }
    }

    return {
        init: function(widget) {
            widget.$.find('.edit').click(function(){
                document.location = $(this).attr('href');
                return false;
            });
            widget.$.find('.delete').click(function(){
                if(confirm('Bы действительно хотите удалить?')){
                    var url = $(this).attr('href');
                    $.post(
                        url,
                        {featureId:widget.$.data('feature-id')},
                        function(){document.location = widget.$.find('.delete').data('project-url');}
                    );
                }
                return false;
            });
            widget.$.on('click', function(event) {
                var featureId = $(this).data('feature-id');
                //_addTodo(data, widget);
                module.trigger('feature_selected', {id:featureId});
                widget.$.addClass('selected');
                $('#feature-score').show();
                return false;
            });
        },
        listeners: {
            // when another widget selected - deselect current
            'feature_selected': _deselect
        }
    }
});