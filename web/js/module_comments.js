window.modlr.defineModule('comments', function(module){
    var $ = module.$;

    function _loadComments(event, widget) {
        var url = widget.$.data('url');
        url = url.replace('--ID--', event.id);
        widget.$.data('last-url', url);
        $.get(url, function(widgetContent){
            var newWidget = module.replaceWidget(widget, widgetContent);
        });
    }

    function _reloadWidget(widget) {
        $.get(widget.$.data('last-url'), function(widgetContent){
            var newWidget = module.replaceWidget(widget, widgetContent);
        });
    }

    return {
        init: function(widget) {
            widget.$.find('.delete').click(function(event) {
                event.preventDefault();
                if(confirm('Bы действительно хотите удалить?')){
                    var url = $(this).attr('href');
                    $.post(
                        url,
                        {commentID:$(this).data('comment-id')},
                        function(resp){module.replaceWidget(widget, resp);}
                    );
                }
            });
            widget.$.find('form').on('submit', function(event) {
                event.preventDefault();
                var $form = $(this);
                $.post($form.attr('action'), $form.serialize(), function(resp){
                    //_reloadWidget(widget);
                    module.replaceWidget(widget, resp);
                });
                return false;
            });
        },
        listeners: {
            // when another widget selected - deselect current
            'feature_selected': _loadComments
        }
    }
});