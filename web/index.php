<?php
require_once(__DIR__ . '/../app/bootstrap.php');
require_once(__DIR__ . '/../app/repositories.php');
require_once(__DIR__ . '/../app/controllers.php');
require_once(__DIR__ . '/../app/routes.php');
$app->run();