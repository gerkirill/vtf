<?php
namespace Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

class FeatureController {

    /** @var \Twig_Environment  */
    private $twig;

    /** @var  Request */
    private $request;

    /** @var UrlGenerator */
    private $urlGenerator;

    /** @var  \Repository\FeatureRepository */
    private $featureRepository;

    /** @var  \Repository\ProjectRepository */
    private $projectRepository;

    /** @var  \Repository\UserRepository */
    private $userRepository;
    
    /** @var  \Symfony\Component\HttpFoundation\Session\Session */
    private $session;

    /** @var  array current user */
    private $user;

    public function __construct($app) {
        $this->twig = $app['twig'];
        $this->request = $app['request'];
        $this->urlGenerator = $app['url_generator'];
        $this->featureRepository = $app['features.repository'];
        $this->projectRepository = $app['projects.repository'];
        $this->userRepository = $app['users.repository'];
        $this->session = $app['session'];
        $this->user = $this->session->get('user');
    }

    public function newScreenAction($projectId) {
        $project = $this->getCurrentUserProject($projectId);
        return $this->twig->render('feature/new.twig', array(
            'project' => $project
        ));
    }

    public function newAction($projectId) {
        $project = $this->getCurrentUserProject($projectId);
        $name = $this->request->get('name');
        $description = $this->request->get('description');
        $url = $this->request->get('url');
        $errors = array();
        if ('' === trim($name)) $errors['name'] = true;
        if ('' === trim($description)) $errors['description'] = true;
        if ($url = trim($url)) {
            if (0 !== strpos($url, 'http')) $url = 'http://'.$url;
            if (!@file_get_contents($url)) $errors['url'] = true;
        }
        if (!empty($errors)) {
            return $this->twig->render('feature/new.twig', array(
                'project' => $project,
                'errors' => $errors,
                'name' => $name, 'description' => $description, 'url' => $url
            ));
        }
        $this->featureRepository->add($projectId, $this->user['id'], $name, $description, $url);
        return new RedirectResponse($this->urlGenerator->generate('project_view',
            array('id'=>$project['id'])
        ));
    }
    
    public function editScreenAction($projectId, $featureId) {
        $project = $this->getCurrentUserProject($projectId);
        $feature = $this->featureRepository->findById($featureId);
        $name = $feature['name'];
        $description = $feature['description'];
        $url = $feature['url'];
        return $this->twig->render('feature/new.twig', array(
            'project' => $project,
            'name' => $name, 'description' => $description, 'url' => $url,
            'edit' => true
        ));
    }
    
    public function editAction($projectId, $featureId) {
        $project = $this->getCurrentUserProject($projectId);
        $feature = $this->featureRepository->findById($featureId);
        $name = $this->request->get('name');
        $description = $this->request->get('description');
        $url = $this->request->get('url');
        $errors = array();
        if ($feature['id_author'] != $this->user['id']) $errors['author'] = true;
        if ('' === trim($name)) $errors['name'] = true;
        if ('' === trim($description)) $errors['description'] = true;
        if ($url = trim($url)) {
            if (0 !== strpos($url, 'http')) $url = 'http://'.$url;
            if (!@file_get_contents($url)) $errors['url'] = true;
        }
        if (!empty($errors)) {
            return $this->twig->render('feature/new.twig', array(
                'project' => $project,
                'errors' => $errors,
                'name' => $name, 'description' => $description, 'url' => $url
            ));
        }
        $this->featureRepository->update($featureId, $name, $description, $url);
        return new RedirectResponse($this->urlGenerator->generate('project_view',
            array('id'=>$project['id'])
        ));
    }

    public function viewAction($id) {
        $feature = $this->featureRepository->findById($id);
        if (!$feature) throw new NotFoundHttpException(sprintf('Feature #%d not found', $id));
        $projectId = $feature['id_project'];
        // this will check if user has access to the feature project
        $project = $this->getCurrentUserProject($projectId);
        $author = $this->userRepository->findById($feature['id_author']);
        $avatar = $this->userRepository->getAvatar($author['email']);
        return $this->twig->render('feature/view.twig', array(
            'feature' => $feature,
            'project' => $project,
            'author' => $author,
            'avatar' => $avatar,
            'can_manage' => $feature['id_author'] === $this->user['id']
        ));
    }

    public function indexAction($projectId) {
        // this will check if user has access to the feature project
        $project = $this->getCurrentUserProject($projectId);
        $features = $this->featureRepository->findAllByProjectId($projectId);
        return $this->twig->render('feature/index.twig', array(
            'features' => $features,
            'project' => $project
        ));
    }

    private function getCurrentUserProject($projectId) {
        // check user access to the project
        try {
            $project = $this->projectRepository->getUserProject($this->user['id'], $projectId);
        } catch (\RuntimeException $e) {
            throw new NotFoundHttpException('Такого проекта у вас нет.');
        }
        return $project;
    }
    
    public function deleteAction() {
        $featureId = $this->request->get('featureId');
        $feature = $this->featureRepository->findById($featureId);
        $status = 'error';
        if ($feature['id_author'] == $this->user['id']) {
            $this->featureRepository->delete($featureId);
            $status = 'ok';
        }
        return new JsonResponse(array('status' => $status));
    }
}