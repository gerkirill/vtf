<?php
namespace Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

class UserController {

    /** @var \Twig_Environment  */
    private $twig;
    /** @var  Request */
    private $request;

    /** @var UrlGenerator */
    private $urlGenerator;

    /** @var  \Repository\UserRepository */
    private $userRepository;

    /** @var  \Symfony\Component\HttpFoundation\Session\Session */
    private $session;
    
    /** @var  \Repository\ProjectRepository */
    private $projectRepository;
    
    /** @var  \Repository\InvitationRepository */
    private $invitationRepository;

    public function __construct($app) {
        $this->twig = $app['twig'];
        $this->request = $app['request'];
        $this->urlGenerator = $app['url_generator'];
        $this->userRepository = $app['users.repository'];
        $this->session = $app['session'];
        $this->projectRepository = $app['projects.repository'];
    }

    public function loginScreenAction() {
        if($this->request->isXmlHttpRequest()){
            return '<script>window.location.href = "'.$this->urlGenerator->generate('login').'"</script>';
        }
        return $this->twig->render('user/login.twig');
    }

    public function loginAction() {
        $email     = $this->request->get('email');
        $password  = $this->request->get('password');
        $user = $this->userRepository->findByEmail($email);
        if ($user && $user['password']===$password) {
            $this->session->set('user', $user);
            if($this->session->get('code')) return new RedirectResponse($this->urlGenerator->generate('invitation', array('code' => $this->session->get('code'))));
            return $this->redirectAfterLoginOk();
        }
        return $this->twig->render('user/login.twig', array(
            'error' => true,
            'email' => $email
        ));
    }

    public function logoutAction() {
        $this->session->remove('user');
        return new RedirectResponse($this->urlGenerator->generate('login'));
    }

    public function registerScreenAction() {
        return $this->twig->render('user/register.twig');
    }

    public function registerAction() {
        $email     = $this->request->get('email');
        $firstname = $this->request->get('firstname');
        $lastname  = $this->request->get('lastname');
        $password  = $this->request->get('password');

        $errors = array();
        // check email - @ and not empty, unique
        if (false === strpos($email, '@')) $errors['email'] = true;
        if ($this->userRepository->findByEmail($email)) $errors['unique'] = true;
        // check fn, ln - not empty
        if ('' == trim($firstname)) $errors['firstname'] = true;
        if ('' == trim($lastname)) $errors['lastname'] = true;
        // check pass - not empty
        if ('' == trim($password)) $errors['password'] = true;

        if (!empty($errors)) {
            // if there are some errors -> render signup form with them
            return $this->twig->render('user/register.twig', array(
                'errors'=> $errors,
                'email' => $email,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'password' => $password
            ));
        }
        // if everything is ok redirect to projects
        $this->userRepository->add($email, $firstname, $lastname, $password);
        $this->session->set('user', $this->userRepository->findByEmail($email));
        if($this->session->get('code')) return new RedirectResponse($this->urlGenerator->generate('invitation', array('code' => $this->session->get('code'))));
        return $this->redirectAfterLoginOk();
    }

    private function redirectAfterLoginOk() {
        return new RedirectResponse($this->urlGenerator->generate('home'));
    }
    
    public function menuScreenAction() {
        $user = $this->session->get('user');
        $projects = array();
        if(!empty($user)){
            $projects = $this->projectRepository->findAllByUserId($user['id']);
        }         
        return $this->twig->render('user/menu.twig', array(
            'projects' => $projects
        ));
    }
}