<?php
namespace Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

class RatingController {
    
    /** @var  Request */
    private $request;

    /** @var  \Repository\RatingRepository */
    private $ratingRepository;
    
    /** @var  \Repository\FeatureRepository */
    private $featureRepository;

    /** @var  \Symfony\Component\HttpFoundation\Session\Session */
    private $session;

    /** @var  array current user */
    private $user;

    public function __construct($app) {
        $this->request = $app['request'];
        $this->urlGenerator = $app['url_generator'];
        $this->ratingRepository = $app['ratings.repository'];
        $this->featureRepository = $app['features.repository'];
        $this->session = $app['session'];
        $this->user = $this->session->get('user');
    }
    
    public function indexAction() {
        $id_feature = $this->request->get('featureID');
        $id_author = $this->request->get('userId');
        $score = $this->request->get('score');
        $feature = $this->featureRepository->findById($id_feature);
        
        $rating = $this->ratingRepository->getRatingForFeature($id_feature, $id_author);
        $agg_rating_count = $feature['agg_rating_count'];
        if(!$rating){
            $agg_rating_count = $feature['agg_rating_count'] + 1;
            $this->ratingRepository->add($id_feature, $id_author, $score);
        }else{
            $this->ratingRepository->update($score, $rating['id']);
        }
        $rating_avg = $this->ratingRepository->getAverageRating($id_feature);
        $this->featureRepository->updateRatingForFeature($id_feature, $rating_avg, $agg_rating_count);
        $mes = array('score' => $rating_avg, 'status' => 'ok');
        return new JsonResponse($mes);
    }
    
    public function getRatingAction() {
        $id_feature = $this->request->get('featureID');
        $rating = $this->ratingRepository->getRatingForFeature($id_feature, $this->user['id']);
        
        $mes = array('score' => $rating['score'], 'userId' => $this->user['id']);
        return new JsonResponse($mes);
    }
}