<?php
namespace Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

class InvitationController {

    /** @var \Twig_Environment  */
    private $twig;

    /** @var  Request */
    private $request;

    /** @var UrlGenerator */
    private $urlGenerator;
    
    /** @var  \Repository\InvitationRepository */
    private $invitationRepository;
    
    /** @var  \Repository\UserRepository */
    private $userRepository;
    
    /** @var  \Repository\ProjectRepository */
    private $projectRepository;
    
    /** @var  \Symfony\Component\HttpFoundation\Session\Session */
    private $session;

    /** @var  array current user */
    private $user;
    
    public function __construct($app) {
        $this->twig = $app['twig'];
        $this->request = $app['request'];
        $this->urlGenerator = $app['url_generator'];
        $this->invitationRepository = $app['invitations.repository'];
        $this->userRepository = $app['users.repository'];
        $this->projectRepository = $app['projects.repository'];
        $this->session = $app['session'];
        $this->user = $this->session->get('user');
    }

    /**
     * Allows project member to invite another user into the project
     * @return JsonResponse
     *
     */
    public function inviteAction() {
        $email = $this->request->get('email');
        $projectId = $this->request->get('projectId');
        $response = array('status' => 'error');
        // check if current user is permitted to invite someone or at least access the project himself
        try {
            $this->projectRepository->getUserProject($this->user['id'], $projectId);
        } catch(\RuntimeException $e) {
            return new JsonResponse($response);
        }
        // check if invitation recipient already there
        $registeredUser = $this->userRepository->findByEmail($email);
            // if user exists - just add him to the project
            // unless he already has access
        if ($registeredUser) {
            // todo: roles to constants
            $this->projectRepository->addProjectMember($projectId, $registeredUser['id'], 'member');
            $response['status'] = 'ok';
        } else {
            // otherwise create invitation record, and wait until user account will be created or user
            // will login with another e-mail
            $code = uniqid("key_");
            $mailingResult = $this->invitationRepository->send($email, $code);
            /* for now - add record despite e-mail status */
            if($mailingResult){
                // checks if there is some other invitation for this e-mail / project combination
                $this->invitationRepository->add($email, $code, $projectId, $this->user['id']);
                $response['status'] = 'ok';
            }
        }
        return new JsonResponse($response);
    }
    
    public function indexAction($code) {
        // if user is not logged in - remember the code and redirect hit to login screen.
        // there user can choose to sign-up instead if he is new to the service
        // TODO: detect if the user already signed-up by the e-mail and redirect to the proper
        // screen (login or sign-up)
        if(!$this->session->get('user')){
            $this->session->set('code', $code);
            return new RedirectResponse($this->urlGenerator->generate('login'));
        }
        // below happens if user is already logged-in
        $this->session->remove('code');
        $invitation = $this->invitationRepository->getByCode($code);
        // add access to the project
        $this->projectRepository->addProjectMember($invitation['id_project'], $this->user['id'], 'member');
        $this->invitationRepository->delete($invitation['id']);
        // redirect to the project page
        return new RedirectResponse($this->urlGenerator->generate('project_view', array(
            'id' => $invitation['id_project']
        )));
    }
}