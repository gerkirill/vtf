<?php
namespace Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGenerator;

class ProjectController {

    /** @var \Twig_Environment  */
    private $twig;

    /** @var  Request */
    private $request;

    /** @var UrlGenerator */
    private $urlGenerator;

    /** @var  \Repository\ProjectRepository */
    private $projectRepository;
    
    /** @var  \Repository\FeatureRepository */
    private $featureRepository;
    
    /** @var  \Symfony\Component\HttpFoundation\Session\Session */
    private $session;
    
    /** @var  array current user */
    private $user;

    public function __construct($app) {
        $this->twig = $app['twig'];
        $this->request = $app['request'];
        $this->urlGenerator = $app['url_generator'];
        $this->projectRepository = $app['projects.repository'];
        $this->featureRepository = $app['features.repository'];
        $this->session = $app['session'];
        $this->user = $this->session->get('user');
    }

    public function viewAction($id) {
        //$project = $this->projectRepository->findById($id);
        //if (!$project) throw new NotFoundHttpException('Такого проекта у вас нет.');
        // check user access to the project
        try {
            $project = $this->projectRepository->getUserProject($this->user['id'], $id);
        } catch (\RuntimeException $e) {
            throw new NotFoundHttpException('Такого проекта у вас нет.');
        }
        return $this->twig->render('project/view.twig', array(
            'project' => $project,
            'userId' => $this->user['id'],
        ));
    }
    
    public function projectNewScreenAction() {
        return $this->twig->render('project/new.twig');
    }
    
    public function projectNewAction() {
        $name = $this->request->get('project_name');
        
        $errors = array();
        // check n - not empty
        if ('' == trim($name)) $errors['name'] = true;
        if (empty($this->user)) $errors['login'] = true;
        
        if (!empty($errors)) {
            // if there are some errors -> render project form with them
            return $this->twig->render('project/new.twig', array('project_name'=> $name));
        }
        // if everything is ok redirect to project
        $projectId = $this->projectRepository->add($name);
        $this->projectRepository->addProjectMember($projectId, $this->user['id'], 'owner');
        return new RedirectResponse($this->urlGenerator->generate('project_view', array(
            'id' => $projectId
        )));
    }
}