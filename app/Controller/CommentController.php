<?php
namespace Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGenerator;

class CommentController {

    /** @var \Twig_Environment  */
    private $twig;

    /** @var  Request */
    private $request;

    /** @var UrlGenerator */
    private $urlGenerator;

    /** @var  \Repository\FeatureRepository */
    private $featureRepository;

    /** @var  \Repository\ProjectRepository */
    private $projectRepository;

    /** @var  \Repository\UserRepository */
    private $userRepository;

    /** @var  \Repository\CommentRepository */
    private $commentRepository;

    /** @var  \Symfony\Component\HttpFoundation\Session\Session */
    private $session;

    /** @var  array current user */
    private $user;

    public function __construct($app) {
        $this->twig = $app['twig'];
        $this->request = $app['request'];
        $this->urlGenerator = $app['url_generator'];
        $this->featureRepository = $app['features.repository'];
        $this->projectRepository = $app['projects.repository'];
        $this->userRepository = $app['users.repository'];
        $this->session = $app['session'];
        $this->user = $this->session->get('user');
        $this->commentRepository = $app['comments.repository'];
    }

    /**
     * Add a comment to the feature
     * @param int $featureId
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction($featureId) {
        // check comment text
        $commentText = trim($this->request->get('comment'));
        if ('' === $commentText) {
            throw new BadRequestHttpException('Комментарий не может быть пустым');
        }
        // check feature existence and access to the project
        $feature = $this->featureRepository->findById($featureId);
        if (!$feature) throw new NotFoundHttpException('Такая фича не найдена');
        $this->checkUserAccessToProject($feature['id_project']);
        // store comment
        $this->commentRepository->add($featureId, $this->user['id'], $commentText);
        return new RedirectResponse($this->urlGenerator->generate('comment_list', array(
            'featureId' => $featureId
        )));
    }

    public function indexAction($featureId) {
        // check feature existence and access to the project
        $feature = $this->featureRepository->findById($featureId);
        if (!$feature) throw new NotFoundHttpException('Такая фича не найдена');
        $this->checkUserAccessToProject($feature['id_project']);
        $comments = $this->commentRepository->findAllByFeatureId($featureId);
        for($i=0; $i<count($comments); $i++){
            $user = $this->userRepository->findById($comments[$i]['id_author']);
            $comments[$i]['avatar'] = $this->userRepository->getAvatar($user['email']);
            $comments[$i]['can_manage'] = false;
            if($comments[$i]['id_author'] === $this->user['id']) $comments[$i]['can_manage'] = true;
        }
        return $this->twig->render('comment/index.twig', array(
            'feature' => $feature,
            'comments' => $comments
        ));
    }
    
    public function deleteAction($featureId) {
        $commentID = $this->request->get('commentID');
        $comment = $this->commentRepository->findById($commentID);
        if ($comment['id_author'] == $this->user['id']) {
            $this->commentRepository->delete($commentID);
        }
        return new RedirectResponse($this->urlGenerator->generate('comment_list', array(
            'featureId' => $featureId
        )));
    }

    private function checkUserAccessToProject($projectId) {
        try {
            $project = $this->projectRepository->getUserProject($this->user['id'], $projectId);
        } catch (\RuntimeException $e) {
            throw new NotFoundHttpException('Такого проекта у вас нет.');
        }
        return $project;
    }
}