<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// initial setup
error_reporting(E_ALL);
ini_set('display_errors', 'on');
require_once __DIR__ . '/../vendor/autoload.php';
$app = new Silex\Application();
$app['debug'] = true;

// Whoops error handler
if ($app['debug']) {
    $app->register(new Whoops\Provider\Silex\WhoopsServiceProvider);
}

// templating
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views'
));

// database
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => 'vtf',
        'user'      => 'insign',
        'password'  => 'insign99',
        'charset'   => 'utf8'
    ),
));

// session
$app->register(new Silex\Provider\SessionServiceProvider());

// controllers as services
$app->register(new Silex\Provider\ServiceControllerServiceProvider());

// url generation
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->before(function (Request $request) use ($app) {
    // add global info about current user
    $app['twig']->addGlobal('current_user', $app['session']->get('user', null));
});

//$app->after(function (Request $request, Response $response) {
//    $response->setCharset('utf-8');
//});