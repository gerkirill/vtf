<?php

$app['users.controller'] = $app->share(function() use ($app) {
    return new \Controller\UserController($app);
});

$app['projects.controller'] = $app->share(function() use ($app) {
    return new \Controller\ProjectController($app);
});

$app['features.controller'] = $app->share(function() use ($app) {
    return new \Controller\FeatureController($app);
});

$app['comments.controller'] = $app->share(function() use ($app) {
    return new \Controller\CommentController($app);
});

$app['invitations.controller'] = $app->share(function() use ($app) {
    return new \Controller\InvitationController($app);
});

$app['ratings.controller'] = $app->share(function() use ($app) {
    return new \Controller\RatingController($app);
});