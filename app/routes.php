<?php

// security - require login for all pages except some exceptions
$app->before(function (Symfony\Component\HttpFoundation\Request $request) use ($app) {
    // named routes which do not require login
    $publicRoutes = array(
        'home',
        'login',
        'login_target',
        'register',
        'register_target',
        'invitation'
    );
    $route = $app['request']->attributes->get('_route');
    if (!in_array($route, $publicRoutes) && !$app['session']->get('user')) {
        return $app->redirect($app['url_generator']->generate('login'));
    }
});

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.twig');
})->bind('home');

// login screen
$app->get('/login', 'users.controller:loginScreenAction')
    ->bind('login');
$app->post('/login', 'users.controller:loginAction')
    ->bind('login_target');
$app->get('/logout', 'users.controller:logoutAction')
    ->bind('logout');

// sign-up screen
$app->get('/register', 'users.controller:registerScreenAction')
    ->bind('register');
$app->post('/register', 'users.controller:registerAction')
    ->bind('register_target');

$app->get('/project/view/{id}', 'projects.controller:viewAction')
    ->bind('project_view');

// projects new
$app->get('/project/new', 'projects.controller:projectNewScreenAction')
    ->bind('project_new');
$app->post('/project/new', 'projects.controller:projectNewAction');

// main menu bar
$app->get('/menu', 'users.controller:menuScreenAction')
    ->bind('menu');

// add new feature to the project
$app->get('/project/{projectId}/feature/new', 'features.controller:newScreenAction')
    ->bind('feature_new');
$app->post('/project/{projectId}/feature/new', 'features.controller:newAction');

$app->get('/project/{projectId}/feature/list', 'features.controller:indexAction')
    ->bind('feature_list');

$app->get('/feature/{id}', 'features.controller:viewAction')
    ->bind('feature_view');

$app->post('/feature/{featureId}/comment/new', 'comments.controller:newAction')
    ->bind('comment_new');
    
$app->post('/feature/{featureId}/comment/delete', 'comments.controller:deleteAction')
    ->bind('comment_delete');
    
$app->get('/feature/{featureId}/comment/list', 'comments.controller:indexAction')
    ->bind('comment_list');

$app->get('/feature/{projectId}/feature/edit/{featureId}', 'features.controller:editScreenAction')
    ->bind('feature_edit');
    
$app->post('/feature/{projectId}/feature/edit/{featureId}', 'features.controller:editAction');

$app->post('/feature/{projectId}/delete', 'features.controller:deleteAction')
    ->bind('feature_delete');
    
//invitation
$app->post('/invitation/send', 'invitations.controller:inviteAction')
    ->bind('invitation_send');
$app->get('/invitation/accept/{code}', 'invitations.controller:indexAction')
    ->bind('invitation');
    
// rating
$app->post('/rating', 'ratings.controller:indexAction')
    ->bind('rating');
$app->post('/rating/get', 'ratings.controller:getRatingAction')
    ->bind('rating_get');