<?php
namespace Repository;

class UserRepository {

    /** @var \Doctrine\DBAL\Connection */
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function findById($id) {
        return $this->db->fetchAssoc('SELECT *
        FROM users
            WHERE id=?',
            array($id));
    }

    public function add($email, $firstname, $lastname, $password) {
        $this->db->insert('users', array(
            'email'     => $email,
            'firstname' => $firstname,
            'lastname'  => $lastname,
            'password'  => $password
        ));
    }

    public function findByEmail($email) {
        $users = $this->db->fetchAll('SELECT * FROM users WHERE email=?', array($email));
        return count($users) ? $users[0] : null;
    }
    
    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $email The email address
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     * @param boole $img True to return a complete IMG tag False for just the URL
     * @param array $atts Optional, additional key/value attributes to include in the IMG tag
     * @return String containing either just a URL or a complete image tag
     * @source http://gravatar.com/site/implement/images/php/
     */
    public function getAvatar( $email, $s = 64, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
}
}