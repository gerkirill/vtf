<?php
namespace Repository;

class FeatureRepository {

    /** @var \Doctrine\DBAL\Connection */
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function findById($id) {
        return $this->db->fetchAssoc('
            SELECT *
            FROM features
            WHERE id=?',
            array($id)
        );
    }

    public function add($projectId, $authorId, $name, $description, $url='')
    {
        $this->db->insert('features', array(
            'id_project' => $projectId,
            'id_author' => $authorId,
            'date_add' => date('Y-m-d H:i:s'),
            'date_edit' => date('Y-m-d H:i:s'),
            // ================================
            'name' => $name,
            'description' => $description,
            'url' => $url,
            'agg_rating_avg' => 0,
            'agg_rating_count' => 0
        ));
        return $this->db->fetchColumn('SELECT last_insert_id()');
    }

    public function findAllByProjectId($projectId) {
        $features = $this->db->fetchAll('
            SELECT *
            FROM features
            WHERE id_project=?
            ORDER BY date_edit DESC',
            array($projectId)
        );
        return $features;
    }
    
    public function updateRatingForFeature($id, $agg_rating_avg, $agg_rating_count) {
        $this->db->update(
            'features', 
            array(
                'agg_rating_avg'=> $agg_rating_avg, 
                'agg_rating_count'=> $agg_rating_count, 
                'date_edit' => date('Y-m-d H:i:s')
            ), 
            array('id' => $id));
    }
    
    public function update($id, $name, $description, $url='') {
        $this->db->update(
            'features', 
            array(
                'name'=> $name, 
                'description'=> $description,
                'url'=> $url,
                'date_edit' => date('Y-m-d H:i:s')
            ), 
            array('id' => $id));
    }
    
    public function delete($id)
    {
        $this->db->delete('features', array('id' => $id));
    }

//    public function findAllByUserId($uid) {
//        $projects = $this->db->fetchAll('
//            SELECT p.*
//            FROM projects p
//            LEFT JOIN project_members pm ON pm.id_project=p.id
//            WHERE pm.id_user=?',
//            array($uid)
//        );
//        return $projects;
//    }
//
//    public function add($name) {
//        $this->db->insert('projects', array(
//            'name'     => $name
//        ));
//          return $this->db->fetchColumn('SELECT last_insert_id()');
//    }
//
//     public function addProjectMember($projectId, $userId, $role) {
//        $this->db->insert('project_members', array(
//            'id_project'     => $projectId,
//               'id_user'     => $userId,
//               'role'     => $role,
//        ));
//    }
}