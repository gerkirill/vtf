<?php
namespace Repository;

class RatingRepository {

    /** @var \Doctrine\DBAL\Connection */
    private $db;
    
    public function __construct($db) {
        $this->db = $db;
    }
    
    public function add($id_feature, $id_author, $score) {
        $this->db->insert('ratings', array(
            'id_feature' => $id_feature,
            'id_author'  => $id_author,
            'score'      => $score,
            'date_add'   => date('Y-m-d H:i:s'),
            'date_edit'  => date('Y-m-d H:i:s')
        ));
    }
    
    public function update($score, $id) {
        $this->db->update(
            'ratings', 
            array(
                'score'=> $score, 
                'date_edit' => date('Y-m-d H:i:s')
            ), 
            array('id' => $id));
    }
    
    public function getRatingForFeature($id_feature, $id_author) {
        $ratings = $this->db->fetchAll('
            SELECT r.*
            FROM ratings r
            WHERE r.id_feature=? AND r.id_author=?',
            array($id_feature, $id_author)
        );
        return count($ratings) ? $ratings[0] : null;
    }
    
    public function getAverageRating($id_feature) {
        $ratings = $this->db->fetchAll('
            SELECT r.*
            FROM ratings r
            WHERE r.id_feature=?',
            array($id_feature)
        );
        $score = 0;
        foreach($ratings as $rat){
            $score = $score + $rat['score'];
        }
        $rating = bcdiv($score, count($ratings), 2);
        return count($ratings) ? $rating : 0;
    }
}