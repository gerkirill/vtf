<?php
namespace Repository;

class ProjectRepository {

    /** @var \Doctrine\DBAL\Connection */
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function findAllByUserId($uid) {
        $projects = $this->db->fetchAll('
            SELECT p.*
            FROM projects p
            LEFT JOIN project_members pm ON pm.id_project=p.id
            WHERE pm.id_user=?',
            array($uid)
        );
        return $projects;
    }

    /**
     * @param $uid
     * @param $projectID
     * @return array
     * @throws \RuntimeException
     */
    public function getUserProject($uid, $projectID) {
        $project = null;
        $userProjects = $this->findAllByUserId($uid);
        foreach($userProjects as $userProject) {
            if ($userProject['id'] === $projectID) {
                $project = $userProject;
                break;
            }
        }
        if (null === $project) throw new \RuntimeException(sprintf('User #%d have no project #%d',
            $uid, $projectID
        ));
        return $project;
    }

    public function add($name) {
        $this->db->insert('projects', array(
            'name'     => $name
        ));
          return $this->db->fetchColumn('SELECT last_insert_id()');
    }
     
    public function addProjectMember($projectId, $userId, $role) {
        // check if record with the same user and project already exists
        $existingRecord = $this->db->fetchAssoc('
            SELECT *
            FROM project_members
                WHERE
                    id_project=?
                    AND id_user=?',
            array($projectId, $userId)
        );
        if ($existingRecord) {
            $updatedRecord = $existingRecord;
            $updatedRecord['role'] = $role;
            $this->db->update('project_members', $updatedRecord, array('id'=>$existingRecord['id']));
        } else {
            $this->db->insert('project_members', array(
                'id_project'     => $projectId,
                'id_user'     => $userId,
                'role'     => $role,
            ));
        }
    }
}