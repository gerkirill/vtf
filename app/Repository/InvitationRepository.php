<?php
namespace Repository;

class InvitationRepository {

    /** @var \Doctrine\DBAL\Connection */
    private $db;
    
    public function __construct($db) {
        $this->db = $db;
    }
    
    public function add($email, $code, $projectId, $authorId) {
        $this->db->insert('invitations', array(
            'email'     => $email,
            'code' => $code,
            'id_project'  => $projectId,
            'id_author'  => $authorId,
            'date_add'  => date('Y-m-d H:i:s')
        ));
    }
    
    public function getByCode($code)
    {
        $invitations = $this->db->fetchAll('
            SELECT i.*
            FROM invitations i
            WHERE i.code=?',
            array($code)
        );
        return count($invitations) ? $invitations[0] : null;
    }
    
    public function delete($id)
    {
        $this->db->delete('invitations', array('id' => $id));
    }
    
    public function send($email, $code)
    {
        $message = "Приглашем Вас в проект! <a href='http://vtf/invitation/accept/$code'>Принять приглашение</a>";
        return mail($email, "приглашение в проект", $message);
    }
}