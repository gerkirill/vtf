<?php
namespace Repository;

class CommentRepository {

    /** @var \Doctrine\DBAL\Connection */
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function add($featureId, $authorId, $comment)
    {
        $this->db->insert('comments', array(
            'id_feature' => $featureId,
            'id_author' => $authorId,
            'date_add' => date('Y-m-d H:i:s'),
            'date_edit' => date('Y-m-d H:i:s'),
            // ================================
            'comment' => $comment
        ));
        return $this->db->fetchColumn('SELECT last_insert_id()');
    }

    public function findAllByFeatureId($featureId) {
        $records = $this->db->fetchAll('
            SELECT *
            FROM comments
            WHERE id_feature=?
            ORDER BY date_edit DESC',
            array($featureId)
        );
        return $records;
    }
    
    public function findById($id) {
        return $this->db->fetchAssoc('
            SELECT *
            FROM comments
            WHERE id=?',
            array($id)
        );
    }
    
    public function delete($id)
    {
        $this->db->delete('comments', array('id' => $id));
    }
}