<?php
// repositories
$app['users.repository'] = $app->share(function() use($app) {
    return new Repository\UserRepository($app['db']);
});

$app['projects.repository'] = $app->share(function() use($app) {
    return new Repository\ProjectRepository($app['db']);
});

$app['features.repository'] = $app->share(function() use($app) {
    return new Repository\FeatureRepository($app['db']);
});

$app['users.repository'] = $app->share(function() use($app) {
    return new Repository\UserRepository($app['db']);
});

$app['comments.repository'] = $app->share(function() use($app) {
    return new Repository\CommentRepository($app['db']);
});

$app['invitations.repository'] = $app->share(function() use($app) {
    return new Repository\InvitationRepository($app['db']);
});

$app['ratings.repository'] = $app->share(function() use($app) {
    return new Repository\RatingRepository($app['db']);
});